package com.betsson.interviewtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class InterviewAplication : Application() {

    companion object {
        lateinit var instance: InterviewAplication
    }
    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}