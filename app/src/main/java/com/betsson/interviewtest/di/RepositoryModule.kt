package com.betsson.interviewtest.di

import com.betsson.interviewtest.repository.OddsRepository
import com.betsson.interviewtest.repository.OddsRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun provideODDS(oddsRepository: OddsRepositoryImpl): OddsRepository
}