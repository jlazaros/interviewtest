package com.betsson.interviewtest.model

open class Bet(var type: String, var sellIn: Int, var odds: Int, var image: String)

object BetType{
    const val WINNING_TEAM = "Winning team"
    const val TOTAL_SCORE = "Total score"
    const val PLAYER_PERFORMANCE = "Player performance"
    const val FIRST_GOAL = "First goal scorer"
    const val NUMBER_FOULS = "Number of fouls"
    const val CORNER_KICKS = "Corner kicks"
}

object BetImages{
    const val WINNING_TEAM = "https://i.imgur.com/mx66SBD.jpeg"
    const val TOTAL_SCORE = "https://i.imgur.com/VnPRqcv.jpeg"
    const val PLAYER_PERFORMANCE = "https://i.imgur.com/Urpc00H.jpeg"
    const val FIRST_GOAL = "https://i.imgur.com/Wy94Tt7.jpeg"
    const val NUMBER_FOULS = "https://i.imgur.com/NMLpcKj.jpeg"
    const val CORNER_KICKS = "https://i.imgur.com/TiJ8y5l.jpeg"
}