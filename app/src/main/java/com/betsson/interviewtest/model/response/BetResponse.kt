package com.betsson.interviewtest.model.response

import com.betsson.interviewtest.model.Bet

data class BetResponse(var bets: ArrayList<Bet> = arrayListOf())