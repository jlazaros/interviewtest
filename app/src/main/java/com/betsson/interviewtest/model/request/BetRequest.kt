package com.betsson.interviewtest.model.request

import com.betsson.interviewtest.model.Bet

data class BetRequest(var bets: ArrayList<Bet> = arrayListOf())