package com.betsson.interviewtest.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

abstract class BaseActivity: AppCompatActivity() {

    private var progressDialog: ProgressDialog = ProgressDialog()
    private var isProgressDialogVisible = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun showProgressDialog() {
        if (!isProgressDialogVisible) {
            progressDialog.isCancelable = false
            isProgressDialogVisible = true
            progressDialog.show(supportFragmentManager, "")
        }
    }

    fun hideProgressDialog() {
        if (isProgressDialogVisible) {
            progressDialog.dismiss()
            isProgressDialogVisible = false
        }
    }

}