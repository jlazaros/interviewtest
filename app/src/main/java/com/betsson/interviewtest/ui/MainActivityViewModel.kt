package com.betsson.interviewtest.ui

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.betsson.interviewtest.R
import com.betsson.interviewtest.model.request.BetRequest
import com.betsson.interviewtest.model.response.BetResponse
import com.betsson.interviewtest.usecase.CalculateOddsUseCase
import com.betsson.interviewtest.usecase.FetchingOddsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    private val fetchingOddsUseCase: FetchingOddsUseCase,
    private val calculateOddsUseCase: CalculateOddsUseCase,
    private val context: Application
): ViewModel() {


    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    private val _showErrorMessage = MutableLiveData<String>()
    val showErrorMessage: LiveData<String>
        get() = _showErrorMessage


    private val _fetchingOddsLiveData: MutableLiveData<BetResponse> =
        MutableLiveData()
    val fetchingOddsLiveData: LiveData<BetResponse>
        get() = _fetchingOddsLiveData

    private val _calculateOddsLiveData: MutableLiveData<BetResponse> =
        MutableLiveData()
    val calculateOddsLiveData: LiveData<BetResponse>
        get() = _calculateOddsLiveData

    init {
        _fetchingOddsLiveData.value = BetResponse()
        _calculateOddsLiveData.value = BetResponse()
    }

    fun fetchingOdds() {
        _loading.value = true
        viewModelScope.launch {
            fetchingOddsUseCase.execute()
                .onSuccess {
                _loading.value = false
                _fetchingOddsLiveData.postValue(it)
                }
                .onFailure {
                 _loading.value = false
                 _showErrorMessage.postValue(context.getString(R.string.internal_server_error))
                }
        }
    }

    fun calculateOdds() {
        val betRequest = BetRequest(_fetchingOddsLiveData.value!!.bets)
        _loading.value = true
        viewModelScope.launch {
            calculateOddsUseCase.execute(betRequest)
                .onSuccess {
                    _loading.value = false
                    _fetchingOddsLiveData.postValue(it)
                }
                .onFailure {
                    _loading.value = false
                    _showErrorMessage.postValue(context.getString(R.string.internal_server_error))
                }
        }
    }
}