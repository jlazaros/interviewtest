package com.betsson.interviewtest.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.betsson.interviewtest.base.BaseActivity
import com.betsson.interviewtest.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainActivityViewModel by viewModels()

    private lateinit var adapter: ItemAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        configListeners()
        configRecyclerView()
        initViewModel()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun initViewModel() {
        viewModel.fetchingOdds()

        viewModel.fetchingOddsLiveData.observe(this) {
                binding.apply {
                    binding.recyclerView.adapter = ItemAdapter(it.bets)
                }
        }

        viewModel.calculateOddsLiveData.observe(this) {
                binding.apply {
                    if(it.bets.isNotEmpty()) {
                        adapter.notifyDataSetChanged()
                    }
                }
        }

        viewModel.showErrorMessage.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
        }

        viewModel.loading.observe(this) {
            if (it)
                showProgressDialog()
            else
                hideProgressDialog()
        }
    }

    private fun configRecyclerView() {
        binding.recyclerView.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
    }

    fun configListeners() {
        binding.button.setOnClickListener {
            viewModel.calculateOdds()
        }
    }

}