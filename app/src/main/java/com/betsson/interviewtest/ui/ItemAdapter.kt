package com.betsson.interviewtest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import coil.clear
import coil.load
import coil.transform.CircleCropTransformation
import com.betsson.interviewtest.R
import com.betsson.interviewtest.model.Bet

class ItemAdapter(var bets: ArrayList<Bet>) : RecyclerView.Adapter<ItemAdapter.ViewHolder>() {

    class ViewHolder(val view: View) : RecyclerView.ViewHolder(view)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_item, parent, false) as View
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return bets.size
    }

    override fun onViewRecycled(holder: ViewHolder) {
        holder.view.findViewById<ImageView>(R.id.image).clear()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = bets[position]

        holder.view.findViewById<TextView>(R.id.text_view_type).text = holder.view.resources.getString(R.string.text_view_type, item.type)
        holder.view.findViewById<TextView>(R.id.text_view_sell).text = holder.view.resources.getString(R.string.text_view_sell, item.sellIn.toString())
        holder.view.findViewById<TextView>(R.id.text_view_odds).text = holder.view.resources.getString(R.string.text_view_odds, item.odds.toString())
        holder.view.findViewById<ImageView>(R.id.image).load(item.image) {
            transformations(CircleCropTransformation())
            placeholder(R.mipmap.ic_launcher_round)
            error(R.drawable.ic_launcher_foreground)
        }
    }

}
