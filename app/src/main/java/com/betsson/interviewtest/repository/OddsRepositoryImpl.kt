package com.betsson.interviewtest.repository

import com.betsson.interviewtest.model.Bet
import com.betsson.interviewtest.model.BetImages
import com.betsson.interviewtest.model.BetType
import com.betsson.interviewtest.model.request.BetRequest
import com.betsson.interviewtest.model.response.BetResponse
import javax.inject.Inject

class OddsRepositoryImpl @Inject constructor(): OddsRepository {

    override suspend fun fetchOdds(): BetResponse {
        return getItemsFromNetwork()
    }

    override suspend fun calculateOdds(request: BetRequest): BetResponse {
        return getCalculationOdds(request)
    }

    fun getItemsFromNetwork(): BetResponse {
        val bets = arrayListOf<Bet>()
        bets.add(Bet(BetType.WINNING_TEAM, 10, 20, BetImages.WINNING_TEAM))
        bets.add(Bet(BetType.TOTAL_SCORE, 2, 0, BetImages.TOTAL_SCORE))
        bets.add(Bet(BetType.PLAYER_PERFORMANCE, 5, 7, BetImages.PLAYER_PERFORMANCE))
        bets.add(Bet(BetType.FIRST_GOAL, 0, 80, BetImages.FIRST_GOAL))
        bets.add(Bet(BetType.NUMBER_FOULS, 5, 49, BetImages.NUMBER_FOULS))
        bets.add(Bet(BetType.CORNER_KICKS, 3, 6, BetImages.CORNER_KICKS))

        return BetResponse(bets)
    }

    fun getCalculationOdds(request: BetRequest): BetResponse {
        val bets = request.bets
        for (i in bets.indices) {
            if (bets[i].type != BetType.TOTAL_SCORE && bets[i].type != BetType.NUMBER_FOULS) {
                if (bets[i].odds > 0) {
                    if (bets[i].type != BetType.FIRST_GOAL) {
                        bets[i].odds = bets[i].odds - 1
                    }
                }
            } else {
                if (bets[i].odds < 50) {
                    bets[i].odds = bets[i].odds + 1

                    if (bets[i].type == BetType.NUMBER_FOULS) {
                        if (bets[i].sellIn < 11) {
                            if (bets[i].odds < 50) {
                                bets[i].odds = bets[i].odds + 1
                            }
                        }

                        if (bets[i].sellIn < 6) {
                            if (bets[i].odds < 50) {
                                bets[i].odds = bets[i].odds + 1
                            }
                        }
                    }
                }
            }

            if (bets[i].type != BetType.FIRST_GOAL) {
                bets[i].sellIn = bets[i].sellIn - 1
            }

            if (bets[i].sellIn < 0) {
                if (bets[i].type != BetType.TOTAL_SCORE) {
                    if (bets[i].type != BetType.NUMBER_FOULS) {
                        if (bets[i].odds > 0) {
                            if (bets[i].type != BetType.FIRST_GOAL) {
                                bets[i].odds = bets[i].odds - 1
                            }
                        }
                    } else {
                        bets[i].odds = bets[i].odds - bets[i].odds
                    }
                } else {
                    if (bets[i].odds < 50) {
                        bets[i].odds = bets[i].odds + 1
                    }
                }
            }
        }

        return BetResponse(bets)
    }

}