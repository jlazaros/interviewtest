package com.betsson.interviewtest.repository

import com.betsson.interviewtest.model.request.BetRequest
import com.betsson.interviewtest.model.response.BetResponse

interface OddsRepository {

    suspend fun fetchOdds(): BetResponse

    suspend fun calculateOdds(request: BetRequest): BetResponse

}