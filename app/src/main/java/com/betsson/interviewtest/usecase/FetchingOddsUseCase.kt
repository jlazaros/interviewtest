package com.betsson.interviewtest.usecase

import com.betsson.interviewtest.model.response.BetResponse
import com.betsson.interviewtest.repository.OddsRepository
import javax.inject.Inject

class FetchingOddsUseCase @Inject constructor(private val repository: OddsRepository) {

    suspend fun execute(): Result<BetResponse> {
        val result = repository.fetchOdds()
            result.let { response ->
                if(response.bets.isNotEmpty()) {
                    //Sort items by sell In - use case
                    result.bets.sortBy { it.sellIn }
                    return Result.success(result)
                } else {
                    return Result.failure(
                        FetchingOddsException.ServerException()
                    )
                }
            }
    }
}

sealed class FetchingOddsException : RuntimeException() {
    class ServerException() : FetchingOddsException()
}