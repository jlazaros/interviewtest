package com.betsson.interviewtest.usecase

import com.betsson.interviewtest.model.request.BetRequest
import com.betsson.interviewtest.model.response.BetResponse
import com.betsson.interviewtest.repository.OddsRepository
import javax.inject.Inject

class CalculateOddsUseCase @Inject constructor(private val repository: OddsRepository) {

    suspend fun execute(request: BetRequest): Result<BetResponse> {
        val result = repository.calculateOdds(request)
        result.let { response ->
            if(response.bets.isNotEmpty()) {
                return Result.success(result)
            } else {
                return Result.failure(
                    FetchingOddsException.ServerException()
                )
            }
        }
    }
}

sealed class CalculateOddsException : RuntimeException() {
    class ServerException() : CalculateOddsException()
}