package com.betsson.interviewtest.ui

import android.app.Application
import com.betsson.interviewtest.model.request.BetRequest
import com.betsson.interviewtest.repository.OddsRepository
import com.betsson.interviewtest.usecase.CalculateOddsUseCase
import com.betsson.interviewtest.usecase.FetchingOddsUseCase
import org.junit.Before
import org.junit.Test
import org.mockito.Mock

class MainActivityViewModelTest {

    @Mock
    private lateinit var oddsRepository: OddsRepository

    @Mock
    private lateinit var calculateOddsUseCase: CalculateOddsUseCase

    @Mock
    private lateinit var fetchingOddsUseCase: FetchingOddsUseCase

    @Mock
    private lateinit var request: BetRequest

    @Mock
    private lateinit var context: Application

    @Mock
    private lateinit var errorMsg: String

    private lateinit var viewModel: MainActivityViewModel

    @Before
    fun setUp() {
        viewModel = MainActivityViewModel(fetchingOddsUseCase, calculateOddsUseCase, context)
    }

    @Test
    fun fetchingOdds() {
        viewModel.fetchingOdds()

        verify(calculateOddsUseCase).fetchOdds()
    }



    @Test
    fun calculateOdds() {
        viewModel.calculateOdds()

        verify(calculateOddsUseCase).calculateOdds(request)
    }
}